/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;

/**
 *
 * @author Eberardo
 */
public class Circulo {
    private int Grosor,continuo,Degradado;
    private Color color,deg;
    private Point sPoint,fPoint;
    private boolean relleno;



    public Circulo(Point start,Point end,Color col,Color degra,int grosor,int Continuo,int Degradado,boolean relleno) {
        setsPoint(start);
        setfPoint(end);
        setColor(col);
        setDeg(degra);
        setGrosor(grosor);
        setRelleno(relleno);
        setContinuo(Continuo);
        setDegradado(Degradado);
    }
    
    public int getRadio(){
        return (getDiam()/2);
    }
    
    public int getDiam(){
        double a,b,A,B,sum,dis;
        
        a=getsPoint().x-getfPoint().x;
        b=getsPoint().y-getfPoint().y;
        A= Math.pow(a, 2);
        B= Math.pow(b, 2);
        
        sum=A+B;
        dis=Math.sqrt(sum);
        System.out.println("La distancia es: "+dis);
        return (int)dis;
    }
    
    public int getAnc(){
        int ancho = fPoint.x-sPoint.x;
        return ancho;
    }
    
    public int getAlt(){
        int alto = fPoint.y-sPoint.y;
        return alto;
    }

    public void pintaCB(Graphics g){
        Graphics2D fig = (Graphics2D) g;
        
        // ESTABLECE EL VALOR DE PREFERENCIA DE LOS ALGORITMOS DE RENDERIZACION
        // PARA EL CONTEXTO GRAFICO, PERMITE MEJORAR O AFINAR LOS GRAFICOS CREADOS
        fig.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        if(getDegradado()==0)
            fig.setColor(color);
        else{
            GradientPaint verticalGradient = new GradientPaint(fPoint.x,fPoint.y, getColor(), 0, getAlt(), getDeg());
            fig.setPaint(verticalGradient);
        }
        if(getContinuo()==0)
            fig.setStroke(new BasicStroke(Grosor));
        else
            fig.setStroke(new BasicStroke(Grosor, // grosor: El definido en el JCombox
                    BasicStroke.CAP_BUTT, // terminación: recta
                    BasicStroke.JOIN_ROUND, // unión: redondeada 
                    1f, // ángulo: 1 grado
                    new float[]{10, 5, 5, 5}, // línea de 10, 5 blancos, línea de 5, 5 blancos
                    2 // fase
            ));
                if (relleno == true) {
                    fig.fillOval(sPoint.x, sPoint.y, getDiam(), getDiam());
                } else {
                    fig.drawOval(sPoint.x, sPoint.y, getDiam(), getDiam());
                }
            
        
        
    }

    public Point getsPoint() {
        return sPoint;
    }

    public void setsPoint(Point sPoint) {
        this.sPoint = sPoint;
    }

    public Point getfPoint() {
        return fPoint;
    }

    public void setfPoint(Point fPoint) {
        this.fPoint = fPoint;
    }
    
    public int getGrosor() {
        return Grosor;
    }

    public void setGrosor(int Grosor) {
        this.Grosor = Grosor;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Color getDeg() {
        return deg;
    }

    public void setDeg(Color deg) {
        this.deg = deg;
    }
    

    

    public boolean isRelleno() {
        return relleno;
    }

    public void setRelleno(boolean relleno) {
        this.relleno = relleno;
    }

    public int getContinuo() {
        return continuo;
    }

    public void setContinuo(int continuo) {
        this.continuo = continuo;
    }

    public int getDegradado() {
        return Degradado;
    }

    public void setDegradado(int Degradado) {
        this.Degradado = Degradado;
    }
}

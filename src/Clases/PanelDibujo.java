/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import static Clases.Interfaz.deg;
import static Clases.Interfaz.dim1;
import static Clases.Interfaz.dim2;
import static Clases.Interfaz.grosor;
import static Clases.Interfaz.opcDeg;
import static Clases.Interfaz.selec;
import static Clases.Interfaz.tipTrazo;
import static Clases.Interfaz.txtCorX;
import static Clases.Interfaz.txtCorY;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;

/**
 *
 * @author eber-
 */
public class PanelDibujo extends JPanel{
    ArrayList <Circulo> ListaCirculos=null;
    ArrayList <Cuadrado> ListaCuadrados=null;
    ArrayList <Rectangulo> ListaRectangulos=null;
    ArrayList <Triangulo> ListaTriangulos=null;
    //ArrayList <Circulo> cirtemp=null;
    Point start, end;
    Timer t1,t2;
    int x,y;
    
    public PanelDibujo(){
        setListaCirculos(new ArrayList<>());
        setListaCuadrados(new ArrayList<>());
        setListaRectangulos(new ArrayList<>());
        setListaTriangulos(new ArrayList<>());
        //setCirtemp(new ArrayList<>());
        //addMouseListener(this);
        setBackground(Color.WHITE);
    }
    
  
    
    @Override
    public void paint (Graphics g){
        super.paint(g);

        getListaCirculos().forEach((objCir) -> {
            objCir.pintaCB(g);
        });
        getListaCuadrados().forEach((objCuad) -> {
            objCuad.pintaCuadrado(g);
        });
        getListaRectangulos().forEach((objRec) -> {
            objRec.pintaRectangulo(g);
        });
        
        getListaTriangulos().forEach((objTri) -> {
            objTri.pintaTriangulo(g);
        });
    }

    public ArrayList<Cuadrado> getListaCuadrados() {
        return ListaCuadrados;
    }

    public void setListaCuadrados(ArrayList<Cuadrado> ListaCuadrados) {
        this.ListaCuadrados = ListaCuadrados;
    }

    public ArrayList<Rectangulo> getListaRectangulos() {
        return ListaRectangulos;
    }

    public void setListaRectangulos(ArrayList<Rectangulo> ListaRectangulos) {
        this.ListaRectangulos = ListaRectangulos;
    }

    public ArrayList<Triangulo> getListaTriangulos() {
        return ListaTriangulos;
    }

    public void setListaTriangulos(ArrayList<Triangulo> ListaTriangulos) {
        this.ListaTriangulos = ListaTriangulos;
    }

    
    public ArrayList<Circulo> getListaCirculos() {
        return ListaCirculos;
    }

    public void setListaCirculos(ArrayList<Circulo> ListaCirculos) {
        this.ListaCirculos = ListaCirculos;
    }
    
    public void pintaCircB(){
        try{
        x = Integer.parseInt(txtCorX.getText());
        y = Integer.parseInt(txtCorY.getText());
        start = new Point(x, y);
        double num = Double.parseDouble(dim1.getText());
        end = new Point (x+(int)num,y+(int)num);
            getListaCirculos().add(new Circulo(start,end,selec,deg,grosor.getValue(),tipTrazo.getSelectedIndex(),opcDeg.getSelectedIndex(),false));
            repaint();
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "El programa solo admite valores numéricos");
        }
    }
    
    public void pintaCuadB(){
        try{
        x = Integer.parseInt(txtCorX.getText());
        y = Integer.parseInt(txtCorY.getText());
        start = new Point(x, y);
        double num = Double.parseDouble(dim1.getText());
        end = new Point (x+(int)num,y+(int)num);
        int n = Integer.parseInt(dim1.getText());
            getListaCuadrados().add(new Cuadrado(start,end,n,selec,deg,grosor.getValue(),tipTrazo.getSelectedIndex(),opcDeg.getSelectedIndex(),false));
            repaint();
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "El programa solo admite valores numéricos");
        }
    }
    
    public void pintaRecB(){
        try{
        x = Integer.parseInt(txtCorX.getText());
        y = Integer.parseInt(txtCorY.getText());
        start = new Point(x, y);
        double num = Double.parseDouble(dim1.getText());
        int num2 = Integer.parseInt(dim2.getText());
        int num3 = Integer.parseInt(dim1.getText());
        end = new Point (x+(int)num,y+(int)num);
            getListaRectangulos().add(new Rectangulo(start,end,num3,num2,selec,deg,grosor.getValue(),tipTrazo.getSelectedIndex(),opcDeg.getSelectedIndex(),false));
            repaint();
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "El programa solo admite valores numéricos");
        }
    }
    
    public void pintaTrianB(){
        try{
        x = Integer.parseInt(txtCorX.getText());
        y = Integer.parseInt(txtCorY.getText());
        start = new Point(x, y);
        double num = Double.parseDouble(dim1.getText());
        end = new Point (x+(int)num,y+(int)num);
        int n = Integer.parseInt(dim1.getText());
            getListaTriangulos().add(new Triangulo(start,end,n,selec,deg,grosor.getValue(),tipTrazo.getSelectedIndex(),opcDeg.getSelectedIndex(),false));
            repaint();
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "El programa solo admite valores numéricos");
        }
    }
    
    public void pintaCircR(){
        try{
        x = Integer.parseInt(txtCorX.getText());
        y = Integer.parseInt(txtCorY.getText());
        start = new Point(x, y);
        double num = Double.parseDouble(dim1.getText());
        end = new Point (x+(int)num,y+(int)num);
            getListaCirculos().add(new Circulo(start,end,selec,deg,grosor.getValue(),tipTrazo.getSelectedIndex(),opcDeg.getSelectedIndex(),true));
            repaint();
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "El programa solo admite valores numéricos");
        }
    }
    
    public void pintaCuadR(){
        try{
        x = Integer.parseInt(txtCorX.getText());
        y = Integer.parseInt(txtCorY.getText());
        start = new Point(x, y);
        double num = Double.parseDouble(dim1.getText());
        end = new Point (x+(int)num,y+(int)num);
        int n = Integer.parseInt(dim1.getText());
            getListaCuadrados().add(new Cuadrado(start,end,n,selec,deg,grosor.getValue(),tipTrazo.getSelectedIndex(),opcDeg.getSelectedIndex(),true));
            repaint();
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "El programa solo admite valores numéricos");
        }
    }
    
    public void pintaRecR(){
        try{
        x = Integer.parseInt(txtCorX.getText());
        y = Integer.parseInt(txtCorY.getText());
        start = new Point(x, y);
        double num = Double.parseDouble(dim1.getText());
        int num2 = Integer.parseInt(dim2.getText());
        int num3 = Integer.parseInt(dim1.getText());
        end = new Point (x+(int)num,y+(int)num);
            getListaRectangulos().add(new Rectangulo(start,end,num3,num2,selec,deg,grosor.getValue(),tipTrazo.getSelectedIndex(),opcDeg.getSelectedIndex(),true));
            repaint();
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "El programa solo admite valores numéricos");
        }
    }
    
    public void pintaTrianR(){
        try{
        x = Integer.parseInt(txtCorX.getText());
        y = Integer.parseInt(txtCorY.getText());
        start = new Point(x, y);
        double num = Double.parseDouble(dim1.getText());
        end = new Point (x+(int)num,y+(int)num);
        int n = Integer.parseInt(dim1.getText());
            getListaTriangulos().add(new Triangulo(start,end,n,selec,deg,grosor.getValue(),tipTrazo.getSelectedIndex(),opcDeg.getSelectedIndex(),true));
            repaint();
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "El programa solo admite valores numéricos");
        }
    }

    public BufferedImage createImage(JPanel panel) {
    int w = panel.getWidth();
    int h = panel.getHeight();
    BufferedImage bi = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
    Graphics2D g = bi.createGraphics();
    panel.paint(g);
    return bi;
    }
}

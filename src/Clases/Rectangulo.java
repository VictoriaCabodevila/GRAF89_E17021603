/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;

/**
 *
 * @author eber-
 */
public class Rectangulo {
    private int Grosor,continuo,Degradado,largo,ancho;
    private Color color,deg;
    private Point sPoint,fPoint;
    private boolean relleno;



    public Rectangulo(Point start,Point end,int Largo,int ancho,Color col,Color degra,int grosor,int Continuo,int Degradado,boolean relleno) {
        setsPoint(start);
        setfPoint(end);
        setLargo(Largo);
        setAncho(ancho);
        setColor(col);
        setDeg(degra);
        setGrosor(grosor);
        setRelleno(relleno);
        setContinuo(Continuo);
        setDegradado(Degradado);
    }

    public int getAncho() {
        return ancho;
    }

    public void setAncho(int ancho) {
        this.ancho = ancho;
    }

    public int getLargo() {
        return largo;
    }

    public void setLargo(int largo) {
        this.largo = largo;
    }
    
    
    public int getAnc(){
        int ancho = fPoint.x-sPoint.x;
        return ancho;
    }
    
    public int getAlt(){
        int alto = fPoint.y-sPoint.y;
        return alto;
    }

    public void pintaRectangulo(Graphics g){
        Graphics2D fig = (Graphics2D) g;
        
        // ESTABLECE EL VALOR DE PREFERENCIA DE LOS ALGORITMOS DE RENDERIZACION
        // PARA EL CONTEXTO GRAFICO, PERMITE MEJORAR O AFINAR LOS GRAFICOS CREADOS
        fig.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        if(getDegradado()==0)
            fig.setColor(color);
        else{
            GradientPaint verticalGradient = new GradientPaint(fPoint.x,fPoint.y, getColor(), 0, getAlt(), getDeg());
            fig.setPaint(verticalGradient);
        }
        if(getContinuo()==0)
            fig.setStroke(new BasicStroke(Grosor));
        else
            fig.setStroke(new BasicStroke(Grosor, // grosor: El definido en el JCombox
                    BasicStroke.CAP_BUTT, // terminación: recta
                    BasicStroke.JOIN_ROUND, // unión: redondeada 
                    1f, // ángulo: 1 grado
                    new float[]{10, 5, 5, 5}, // línea de 10, 5 blancos, línea de 5, 5 blancos
                    2 // fase
            ));
                if (relleno == true) {
                    fig.fillRect(sPoint.x, sPoint.y, getAncho(), getLargo());
                } else {
                    fig.drawRect(sPoint.x, sPoint.y, getAncho(), getLargo());
                }
            
        
        
    }

    public Point getsPoint() {
        return sPoint;
    }

    public void setsPoint(Point sPoint) {
        this.sPoint = sPoint;
    }

    public Point getfPoint() {
        return fPoint;
    }

    public void setfPoint(Point fPoint) {
        this.fPoint = fPoint;
    }
    
    public int getGrosor() {
        return Grosor;
    }

    public void setGrosor(int Grosor) {
        this.Grosor = Grosor;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Color getDeg() {
        return deg;
    }

    public void setDeg(Color deg) {
        this.deg = deg;
    }
    

    

    public boolean isRelleno() {
        return relleno;
    }

    public void setRelleno(boolean relleno) {
        this.relleno = relleno;
    }

    public int getContinuo() {
        return continuo;
    }

    public void setContinuo(int continuo) {
        this.continuo = continuo;
    }

    public int getDegradado() {
        return Degradado;
    }

    public void setDegradado(int Degradado) {
        this.Degradado = Degradado;
    }
}
